# -*- coding: utf-8 -*-

import time
import os

script_dir = os.path.dirname(__file__)


def main():
    """
    Carga la información en los modelos de la app AQS --> mod_lyntia_aqs.models
    """
    print("\nIniciando Descarga...\n")
    print("--> Descargando y procesando Endesa...")
    exec(open(os.path.join(script_dir, "endesa/main.py")).read())
    print("--> Descargando y procesando Iberdrola...")
    exec(open(os.path.join(script_dir, "iberdrola/main.py")).read())
    print("--> Descargando y procesando Naturgy...")
    exec(open(os.path.join(script_dir, "naturgy/main.py")).read())
    print("\nSE han descargado y procesado los datos correctamente.")
    return 1


if __name__ == '__main__':
    main()
