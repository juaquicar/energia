from tabula.io import read_pdf, convert_into
import requests
import wget
import os
import pandas



input = 'data.pdf'
output = 'data.csv'

# Borra si existe
try:
    os.remove("data.pdf")
    os.remove("data.csv")
except:
    print("No existe archivo data.pdf")
    print("No existe archivo data.csv")

# Descarga de Archivo
url = 'https://www.edistribucion.com/content/dam/edistribucion/nodos-de-acceso/EDRD_Capacidad_de_Acceso_2021_07_01.pdf'
wget.download(url, input)


# Conversion a XLS
print ("Convirtiendo en CSV")
df = read_pdf(input, pages='all')
convert_into(input, output, output_format="tsv", pages='all', stream=True)


# Tratamiento de CSV
import csv

file_object = open(output, 'r')
lines = csv.reader(file_object, delimiter='\t', quotechar='"')
flag = 0
data = []

DATA_FILTER = ['', 'Comunidad', 'Autónoma']


data.append(['Comunidad',
             'Provincia',
             'Ubicación',
             'Y',
             'X',
             'Tensión',
             'Disponible',
             'Total',
             '',
             '',
             '',
             ''])



for line in lines:
    if line[0] in DATA_FILTER:
        pass
    else:

        if line[2] == '':
            del line[2]


        data.append(line)

file_object.close()
file_object = open(output, 'w')
for line in data:
    str1 = '\t'.join(line)
    file_object.write(str1 + "\n")
file_object.close()


