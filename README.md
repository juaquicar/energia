# Instalación


Probado en Ubuntu 20.04 Desktop

```bash
sudo apt install default-jre
sudo apt install virtualenv
cd energia
virtualenv env
source env/bin/activate
pip3 install -r requirements.txt
```

# Ejecución

```bash
cd energia
source env/bin/activate
python main.py
```



# TIPO DE EPGS CARGA DE *.CSV

- ENDESA: 4326
- NATURGY: Hay que mirar la provincia y el uso: 25829, 25830 y 25831
- IBERDROLA: 3857



# VERSION

- [06/07/2021] - v1.0   - Liberada versión inicial que descarga y procesa IBERDROLA, ENDESA y NATURGY.

# CONTRIBUTORS

- Juan M. Quijada [quijada.jm@gmail.com]