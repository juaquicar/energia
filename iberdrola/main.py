from tabula.io import read_pdf, convert_into
import requests
import wget
import os
import pandas
import csv


input = 'data.pdf'
output = 'data.csv'

# Borra si existe
try:
    os.remove("data.pdf")
    os.remove("data.csv")
except:
    print("No existe archivo data.pdf")
    print("No existe archivo data.csv")

# Descarga de Archivo
url = 'https://www.i-de.es/socdis/gc/prod/es_ES/contenidos/docs/MapaDeCapacidad_iDE_1_Julio_2021.pdf'
wget.download(url, input)


# Conversion a XLS
print ("Convirtiendo en CSV")
df = read_pdf(input, pages='all')
convert_into(input, output, output_format="tsv", pages='all', stream=True)



# Tratamiento de CSV

file_object = open(output, 'r')
lines = csv.reader(file_object, delimiter='\t', quotechar='"')
flag = 0
data = []

DATA_FILTER = ['', 'Denominacion', 'Denominación']

data.append(['Denominación',
             'X',
             'Y',
             'Tensión',
             'Cap. Disponible',
             'Cap. Ocupada',
             'Cap. No Conectada'])

for line in lines:
    if line[0] in DATA_FILTER:
        pass
    else:
        line[0] = line[0].replace(' ', '_')

        if line[1] == '': # Elimina espacios en blanco
            del line[1]

        if "-" in line[1]: # Elimina los valroes de tension
            pass
        else:
            del line[1]

        line[4] = line[4].replace(',', '.')
        line[4] = line[4].replace('*', '')
        data.append(line)



file_object.close()
file_object = open(output, 'w')
for line in data:
    str1 = '\t'.join(line)
    file_object.write(str1 + "\n")
file_object.close()


