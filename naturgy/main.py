from tabula.io import read_pdf, convert_into
import requests
import wget
import os
import pandas
import csv



TABLE_URLS = [
    {
        'name': 'ASTURIAS',
        'input': 'asturias-data.pdf',
        'output': 'asturias-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Asturias.pdf',
    },
    {
        'name': 'AVILA',
        'input': 'avila-data.pdf',
        'output': 'avila-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Avila.pdf',
    },
    {
        'name': 'BARCELONA',
        'input': 'barcelona-data.pdf',
        'output': 'barcelona-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Barcelona.pdf',
    },
    {
        'name': 'CIUDAD REAL',
        'input': 'ciudadreal-data.pdf',
        'output': 'ciudadreal-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Ciudad Real.pdf',
    },
    {
        'name': 'CUENCA',
        'input': 'cuenca-data.pdf',
        'output': 'cuenca-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Cuenca.pdf',
    },
    {
        'name': 'GUADALAJARA',
        'input': 'guadalajara-data.pdf',
        'output': 'guadalajara-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Guadalajara.pdf',
    },
    {
        'name': 'A CORUNA',
        'input': 'coruna-data.pdf',
        'output': 'coruna-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Coruña.pdf',
    },
    {
        'name': 'LUGO',
        'input': 'lugo-data.pdf',
        'output': 'lugo-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Lugo.pdf',
    },
    {
        'name': 'MADRID',
        'input': 'madrid-data.pdf',
        'output': 'madrid-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Madrid.pdf',
    },
    {
        'name': 'ORENSE',
        'input': 'orense-data.pdf',
        'output': 'orense-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Orense.pdf',
    },
    {
        'name': 'PONTEVEDRA',
        'input': 'pontevedra-data.pdf',
        'output': 'pontevedra-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Pontevedra.pdf',
    },
    {
        'name': 'SEGOVIA',
        'input': 'segovia-data.pdf',
        'output': 'segovia-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Segovia.pdf',
    },
    {
        'name': 'SORIA',
        'input': 'soria-data.pdf',
        'output': 'soria-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Soria.pdf',
    },
    {
        'name': 'TOLEDO',
        'input': 'toledo-data.pdf',
        'output': 'toledo-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Toledo.pdf',
    },
    {
        'name': 'ZAMORA',
        'input': 'zamora-data.pdf',
        'output': 'zamora-data.csv',
        'url': 'https://estaticos.naturgy.com/ufd/Capacidades/Publicacion capacidad Zamora.pdf',
    },
]


# Borrado de Archivos anteriores...
print("Borrando archivos antiguos...")
for item in TABLE_URLS:
    try:
        os.remove(item['input'])
        os.remove(item['output'])
    except:
        print ("No existe " + item['input'])
        print ("No existe " + item['output'])


for item in TABLE_URLS:
    # Descarga de Archivo
    wget.download(item['url'], item['input'])
    # Conversion a XLS
    print("\nConvirtiendo en CSV")
    print(item['output'])
    df = read_pdf(item['input'], pages='all')
    convert_into(item['input'], item['output'], output_format="csv", pages='all', stream=True)


# Procesamiento

DATA_FILTER_1 = ['', 'Subestación', 'Nombre Matrícula', 'Nombre Matrícula X Y', 'Capacidad Subestación Georreferenciación']
DATA_FILTER_2 = ['']



for item in TABLE_URLS:
    data = []
    print("Procesando: " + item['name'])
    file_object = open(item['output'], 'r')
    lines = csv.reader(file_object, delimiter=',', quotechar='"')
    header = [
        'Matricula',
        'X',
        'Y',
        'Tension',
        '',
    ]
    data.append(header)
    for line in lines:
        if line[1] == '':
            del line[1]
        if line[0] in DATA_FILTER_1 or line[1] in DATA_FILTER_2:
            pass
        else:
            try:
                coor = line[1].split()
                del line[1]
                line.insert(1, coor[0])
                line.insert(2, coor[1])
            except:
                del line[1]
            data.append(line)
    file_object.close()
    file_object = open(item['output'], 'w')
    for line in data:
        str1 = '\t'.join(line)
        file_object.write(str1 + "\n")
    file_object.close()




